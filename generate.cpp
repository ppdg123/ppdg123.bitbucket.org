#include <stdio.h>
#include <string.h>
int main(int argc, char * argv[])
{
	char cmd[300];
	int i,j=0;
	FILE * fp = fopen("tmp.html","w"),*f;
	fprintf(fp,"<table>\n");
	for(i=1;i<=75;i++){
	     if(i%2) fprintf(fp,"<tr>\n");
	     fprintf(fp,"<td>\n<img src='./res/%d.jpg' width=640 height=320></td>\n",i);
	     fprintf(fp,"<td>Bottom-up process : <br>");
	     sprintf(cmd,"./res/%d.0.dat",i);
	     f = fopen(cmd,"r");
	     while(NULL!=fgets(cmd,300,f)){
	        fprintf(fp,"%s<br>",cmd);
	     }
	     fclose(f);
	     fprintf(fp,"</td>\n<td>Top-down process : <br>");
	     sprintf(cmd,"./res/%d.1.dat",i);
	     f = fopen(cmd,"r");
	     while(NULL!=fgets(cmd,300,f)){
	        fprintf(fp,"%s<br>",cmd);
	     }
	     fclose(f);
	     fprintf(fp,"</td>\n");
	     if(i%2==0) fprintf(fp,"</tr>\n");
	}
	fprintf(fp,"</table>");
	fclose(fp);
	return 0;
}
